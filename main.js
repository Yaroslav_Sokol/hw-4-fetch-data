// AJAX - Це підхід в програмуванні, який дозволяє нам взаємодіяти з серверами, відправляти на отримувати дані асинхронно, 
// аби не очікувать закінчення якоїсь операції по типу передача даних, ну ао її отримання.


fetch('https://ajax.test-danit.com/api/swapi/films')
.then(response => response.json()) 

.then(data => {
    console.log(data)
    const fetchCharacterData = url => {
        return fetch(url)
        .then(response => response.json())
    };
  
    const fetchCharacters = async (film) => {
        const charactersDataPromises = film.characters.map(fetchCharacterData);
        return Promise.all(charactersDataPromises);
    };
  
    const filmPromises = data.map(fetchCharacters);
  
    Promise.all(filmPromises)
    .then(filmData => {
        const filmList = document.getElementById('filmList');
        filmData.forEach((charactersData, index) => {
            const film = data[index];
            const filmDiv = document.createElement('div');
            filmDiv.classList.add('film')

            const episodeElement = document.createElement('h2');
            episodeElement.textContent = `Номер: ${film.episodeId}`;

            const titleElement = document.createElement('h3');
            titleElement.textContent = `Назва: ${film.name}`;

            const loadingSpinner = document.createElement('div');
            loadingSpinner.classList.add('loading-spinner');
            titleElement.appendChild(loadingSpinner);

            const charactersDataElement = document.createElement('div');
            charactersDataElement.textContent = 'Дані про персонажів:';

            charactersData.forEach((characterData, characterIndex) => {
                const characterInfo = document.createElement('p');
                characterInfo.textContent = `${characterIndex + 1}. ${characterData.name} | День народження: ${characterData.birthYear} | Стать: ${characterData.gender} | Колір волосся: ${characterData.hairColor} | Ріст: ${characterData.height}`;
                charactersDataElement.appendChild(characterInfo);
            });

            const openingCrawlElement = document.createElement('p');
            openingCrawlElement.textContent = `Зміст: ${film.openingCrawl}`;


            filmDiv.appendChild(episodeElement);
            filmDiv.appendChild(titleElement);
            filmDiv.appendChild(charactersDataElement);
            filmDiv.appendChild(openingCrawlElement);
            titleElement.removeChild(loadingSpinner);

            const brElement = document.createElement('br');
            filmDiv.appendChild(brElement);
            filmList.appendChild(filmDiv);
        });
    });
});